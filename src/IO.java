package src;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class IO {
    public static List<String> inputLines(File filename) throws IOException {
        List<String> lines = new ArrayList<String>();
        String line;
        BufferedReader input = null;
        try {
            input = new BufferedReader(new FileReader(filename));
            while ((line = input.readLine()) != null){
                line = line.trim();
                if (line.equals("")) continue;
                lines.add(line);
            }
        }
        catch (IOException e){
            return null;
        }
        finally {
            if (input != null){
                input.close();
            }
            if (lines.isEmpty()) {
                return null;
            }
        }
        return lines;
    }

    public static boolean outputLines(List<String> lines, File file) throws IOException{

        PrintWriter out = null;
        int step=0;
        if ((lines == null) || (lines.isEmpty())){
            return false;
        }
        try{
            out = new PrintWriter(file);
            for (int i = 0; i < lines.size(); i++){
               if(step==3){
                out.print(lines.get(i).trim());
                out.print(",");
                out.println();
                step=0;
               }
               else{
                   out.print(lines.get(i).trim());
                   out.print(",");
                   step++;
               }
            }
        }
        catch (IOException e){
            return false;
        }
        finally {
            if (out != null){
                out.close();
            }
            return true;
        }
    }

}
