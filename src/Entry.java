package src;

public class Entry {

    private String cadNumber;
    private String company;
    private String area;
    private String adress;

    public Entry(){
        this.cadNumber="";
        this.company="";
        this.area="";
        this.adress="";
    }

    public Entry(String cadNumber,String company,String area,String adress){
        this.cadNumber=cadNumber;
        this.company=company;
        this.area=area;
        this.adress=adress;
    }

    public String getCadNumber() {
        return cadNumber;
    }

    public void setCadNumber(String cadNumber) {
        this.cadNumber = cadNumber;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }
}
