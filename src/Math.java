package src;

import java.util.List;

public class Math extends MAIN {

    public static String columnTotalSumm(int column){

        String totalArea;
        double summ=0;
        int tableSize;

        tableSize = mainFrame.table.getRowCount();

        for(int i=0;i<tableSize;i++){
            if(!(mainFrame.table.getValueAt(i,column).equals(""))){
                summ += Double.parseDouble((String) mainFrame.table.getValueAt(i,column));
            }
        }

        totalArea = Double.toString(summ);

        return totalArea;

    }

    public static String columnMaxValue(int column){

        String maxValue;
        double maxValueDouble = 0;
        int tableSize;

        tableSize = mainFrame.table.getRowCount();

        for(int i=0;i<tableSize;i++){
            if(!(mainFrame.table.getValueAt(i,column).equals(""))){
                if(maxValueDouble < Double.parseDouble((String) mainFrame.table.getValueAt(i,column))){
                    maxValueDouble = Double.parseDouble((String) mainFrame.table.getValueAt(i,column));
                }
            }
        }

        maxValue = Double.toString(maxValueDouble);

        return maxValue;

    }

}
