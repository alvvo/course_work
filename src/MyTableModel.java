package src;


import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.util.List;

public class MyTableModel extends MAIN implements javax.swing.table.TableModel {

    RowSorter<MyTableModel> sorter;

    public int getRowCount() {
        return 0;
    }

    public int getColumnCount() {
        return 0;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return null;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        return null;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

    }

    @Override
    public void addTableModelListener(TableModelListener l) {

    }

    @Override
    public void removeTableModelListener(TableModelListener l) {

    }

    public static void addRow(List<Entry> ObjectList){
        Object rowData[] = new Object[4];
        DefaultTableModel defaultTableModel = (DefaultTableModel) mainFrame.table.getModel();
        int size = ObjectList.size();

        for(int i=0;i<size;i++){
            rowData[0] = ObjectList.get(i).getCadNumber();
            rowData[1] = ObjectList.get(i).getCompany();
            rowData[2] = ObjectList.get(i).getArea();
            rowData[3] = ObjectList.get(i).getAdress();
            RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(defaultTableModel);
            mainFrame.table.setRowSorter(sorter);
            defaultTableModel.addRow(rowData);
            /*if(ViewController.filterStatus){
                sorter.toggleSortOrder(1);
                sorter.toggleSortOrder(1);
            }
            else{
                sorter.toggleSortOrder(1);
            }*/

        }
        mainFrame.table.setModel(defaultTableModel);
    }

    public static void resetModel(){
        mainFrame.table.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {

                },
                new String [] {
                        "CAD.NUMBER", "COMPANY", "AREA", "ADDRESS"
                }
        ));
    }

}
