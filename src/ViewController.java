package src;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ViewController extends MAIN{

    File file;
    File newFile;

    List fileDataLines;

    static List<Entry> ObjectsList;
    static List<Entry> ObjectsListAfterFilter;

    static boolean filterStatus;

    Entry entry;
    MyTableModel myTableModel;
    ResultFrame resultFrame;
    WarningFrame warningFrame;

    public ViewController(){
        file = null;
        newFile = null;
        fileDataLines = new ArrayList();
        myTableModel = new MyTableModel();
        ObjectsList = new ArrayList<Entry>();
        filterStatus = false;
    }

    private void fileChooser(){ // Вызов панели с выбором файла и инициализация этого файла
        JFileChooser fileopen = new JFileChooser();
        int ret = fileopen.showOpenDialog(mainFrame);
        if (ret == JFileChooser.APPROVE_OPTION) {
            file = fileopen.getSelectedFile();
        }
    }

    private void saveDataToList() throws IOException, IOException { // Сохраняет информацию в список линий
        fileDataLines = IO.inputLines(file);
    }

    private void fillTableModelFromFile(){
        Iterator iter = fileDataLines.iterator();
        while(iter.hasNext()){
            String str = (String) iter.next();
            String[] str_arr = str.split(",");
            if(     str_arr.length==4
                    &&
                    (str_arr[0]!=null&&
                    str_arr[1]!=null&&
                    str_arr[2]!=null&&
                    str_arr[3]!=null
                    )
                    &&
                    (
                    !(str_arr[0].equals("null"))
                    &&(!(str_arr[1].equals("null"))
                    &&(!(str_arr[2].equals("null"))
                    &&(!(str_arr[3].equals("null"))
                    )
            )))){
                entry = new Entry(str_arr[0],str_arr[1],str_arr[2],str_arr[3]);
                ObjectsList.add(entry);
            }
            else{
                warningFrame = new WarningFrame("Table is not correctly fill");
            }
        }
    }

    private void addRowsToTable(){
        MyTableModel.addRow(ObjectsList);
    }

    private void addRowsToTableWithParam(List<Entry> list){
        MyTableModel.addRow(list);
    }

    private void addNewLine(){
        entry = new Entry();
        ObjectsList.add(entry);
    }

    private void cloneTable(){

        int row = mainFrame.table.getModel().getRowCount();

        for(int line=0;line<row;line++){
            int column = 0;
            entry = new Entry();

            entry.setCadNumber(mainFrame.table.getValueAt(line,column).toString());
            column++;
            entry.setCompany(mainFrame.table.getValueAt(line,column).toString());
            column++;
            entry.setArea(mainFrame.table.getValueAt(line,column).toString());
            column++;
            entry.setAdress(mainFrame.table.getValueAt(line,column).toString());

            if(filterStatus){
                ObjectsListAfterFilter.add(entry);
            }
            else{
                ObjectsList.add(entry);
            }
        }
        if(filterStatus){
            filterActionStop();
        }
    }

    private void saveToLegacyFile() throws IOException {
        if(file==null){
            onSaveAsClick();
        }
        else{
            List<String> output = new ArrayList<>();

            for(int i = 0;i<ObjectsList.size();i++){
                output.add(ObjectsList.get(i).getCadNumber());
                output.add(ObjectsList.get(i).getCompany());
                output.add(ObjectsList.get(i).getArea());
                output.add(ObjectsList.get(i).getAdress());
            }

            IO.outputLines(output,file);
            resultFrame = new ResultFrame("Successfully saved");
        }
    }

    private void saveToNewFile() throws IOException {
        JFileChooser fc = new JFileChooser();
        if ( fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            newFile = fc.getSelectedFile();
        }

        file = newFile;

        List<String> output = new ArrayList<>();

        for(int i = 0;i<ObjectsList.size();i++){
            output.add(ObjectsList.get(i).getCadNumber());
            output.add(ObjectsList.get(i).getCompany());
            output.add(ObjectsList.get(i).getArea());
            output.add(ObjectsList.get(i).getAdress());
        }

        IO.outputLines(output,newFile);
        resultFrame = new ResultFrame("Successfully saved");
    }

    // Actions Click
    public void onSaveAsClick() throws IOException {
        saveToNewFile();
    }

    public void onSaveClick() throws IOException {
        saveToLegacyFile();
    }

    public void onOpenClick() throws IOException {
        ObjectsList = new ArrayList<Entry>();
        fileChooser();
        saveDataToList();
        fillTableModelFromFile();
        MyTableModel.resetModel();
        addRowsToTable();
    }

    public void onCreateNewTableModelClick(){
        ObjectsList = new ArrayList<Entry>();
        MyTableModel.resetModel();

        addNewLine();
        addRowsToTable();

        resultFrame = new ResultFrame("New Table Created!");
    }

    public void onNewLineClick(){
        MyTableModel.resetModel();
        addNewLine();
        addRowsToTable();
    }

    public void onEditEndClick(){
        if(filterStatus){
            cloneTable();
            addRowsToTable();
        }
        else{
            ObjectsList = new ArrayList<Entry>();
            cloneTable();
            MyTableModel.resetModel();
            addRowsToTable();
        }

        resultFrame = new ResultFrame("Changes Applied");
    }

    public void onShowAllClick(){
        if(filterStatus){
            cloneTable();
        }
        else{
            MyTableModel.resetModel();
        }
        addRowsToTable();
    }

    public void onTotalAreaClick(){
        resultFrame = new ResultFrame(Math.columnTotalSumm(2));
    }

    public void onMaxAreaClick(){
        resultFrame = new ResultFrame(Math.columnMaxValue(2));
    }

    public void onDeleteRow(){
        deleteRow();
        if(filterStatus){
            cloneTable();
            addRowsToTable();
        }
        else{
            MyTableModel.resetModel();
            addRowsToTable();
        }
    }

    // Actions Enter
    public void filterActionStart(){
        ObjectsListAfterFilter = new ArrayList<Entry>();
        String filterWord = mainFrame.filterField.getText();

        for(int i=0;i<ObjectsList.size();i++){
            if(ObjectsList.get(i).getCadNumber().equals(filterWord) ||
                    ObjectsList.get(i).getCompany().equals(filterWord) ||
                    ObjectsList.get(i).getArea().equals(filterWord) ||
                    ObjectsList.get(i).getAdress().equals(filterWord)
            )
            {
                ObjectsListAfterFilter.add(ObjectsList.get(i));
                ObjectsList.remove(i);
                i--;
            }

        }
        if(ObjectsListAfterFilter.size()==0){
            MyTableModel.resetModel();
            addRowsToTable();
            warningFrame = new WarningFrame("Not Found");
        }
        else{
            MyTableModel.resetModel();
            addRowsToTableWithParam(ObjectsListAfterFilter);
        }
        filterStatus=true;
        ObjectsListAfterFilter = new ArrayList<>();
    }

    public void filterActionStop(){

        for(int i=0;i<ObjectsListAfterFilter.size();i++){
            ObjectsList.add(ObjectsListAfterFilter.get(i));
        }
        ObjectsListAfterFilter = new ArrayList<>();

        MyTableModel.resetModel();

        filterStatus=false;

    }

    public void deleteRow(){
        if(filterStatus){
            warningFrame = new WarningFrame("Please Turn Off Filtering");
        }
        else{

            String activateWord = mainFrame.combinationField.getText();

            for(int i=0;i<ObjectsList.size();i++){
                if(ObjectsList.get(i).getCadNumber().equals(activateWord) ||
                        ObjectsList.get(i).getCompany().equals(activateWord) ||
                        ObjectsList.get(i).getArea().equals(activateWord) ||
                        ObjectsList.get(i).getAdress().equals(activateWord))
                {
                    ObjectsList.remove(i);
                    i--;
                }
            }

        }

    }

}
